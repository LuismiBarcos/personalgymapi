package validators;

import models.ApiKey;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

public class ActionAuthenticator extends  play.mvc.Security.Authenticator {
	
	@Override
	public String getUsername(Http.Context ctx) {
		String apiKeyHash = ctx.request().getQueryString("api_key");
		if(apiKeyHash != null) {
			ApiKey apiKey = ApiKey.findByHash(apiKeyHash);
			return apiKey == null ? null : "Correct!";
		}
		return null;
	}
	
	@Override
	public Result onUnauthorized(Http.Context ctx) {		
		return play.mvc.Results.status(Http.Status.UNAUTHORIZED, Json.toJson("Not valid api key"));
	}
}
