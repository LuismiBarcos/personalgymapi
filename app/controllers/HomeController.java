package controllers;

import java.util.List;

import models.Exercise;
import models.MuscleGroup;
import play.libs.Json;
import play.mvc.*;
import validators.ActionAuthenticator;


/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
@Security.Authenticated(ActionAuthenticator.class)
public class HomeController extends Controller {
	
    public Result index() {
        return ok("PersonalGYM-API");
    }
    
    public Result retrieveMuscleGroups() {
    	List<MuscleGroup> muscleGroups = MuscleGroup.retrieveAllMuscleGroups().getList();
    	return ok(Json.toJson(muscleGroups));
    }
    
    public Result retrieveExercisesByMuscleGroup(Long muscGroupId) {
    	List<Exercise> exercises = Exercise.retrieveExercisesByMuscleGroup(muscGroupId).getList();
    	return ok(Json.toJson(exercises));
    }
    
    public Result retrieveExercise(Long id) {
    	return ok(Json.toJson(Exercise.retrieveExerciseById(id)));
    }
    
    public Result retrieveRandomExercises() {
    	return ok(Json.toJson(Exercise.retrieveRandomExercises()));
    }

}
