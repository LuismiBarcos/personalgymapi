package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import play.data.validation.Constraints.Required;

@Entity
public class MuscleGroup extends Model{
	public static final Finder<Long, MuscleGroup> find = new Finder<>(MuscleGroup.class);
	
	private static final Integer MAX_ROWS = 100;
	
	@Id
	private Long id;
	@Required
	private String name;
	@Required
	private String imageUrl;
	@JsonIgnore
	@Null
	@OneToMany(cascade=CascadeType.ALL, mappedBy="muscleGroup")
	private List<Exercise> exercises = new ArrayList<Exercise>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<Exercise> getExercises() {
		return exercises;
	}
	public void setExercises(List<Exercise> exercises) {
		this.exercises = exercises;
	}
	
	public static PagedList<MuscleGroup> retrieveAllMuscleGroups(){
		return find.query()
				.setMaxRows(MAX_ROWS)
				.findPagedList();
	}
	
}
