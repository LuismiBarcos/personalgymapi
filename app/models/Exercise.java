package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import io.ebean.Query;
import io.ebean.RawSql;
import io.ebean.RawSqlBuilder;
import play.data.validation.Constraints.Required;

@Entity
public class Exercise extends Model{
	
	public static final Integer MAX_ROWS = 15;
	public static final Finder<Long, Exercise> find = new Finder<>(Exercise.class);
	
	@Id
	private Long id;
	@Required
	private String name;
	@Required
	private String imageUrl;
	@Required
	@Column(columnDefinition = "varchar(15000)")
	private String description;
	@Required
	@JsonIgnore
	@ManyToOne
	private MuscleGroup muscleGroup;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}
	
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public MuscleGroup getMuscleGroup() {
		return muscleGroup;
	}
	
	public void setMuscleGroup(MuscleGroup muscleGroup) {
		this.muscleGroup = muscleGroup;
	}
	
	public static PagedList<Exercise> retrieveExercisesByMuscleGroup(Long muscGroupId) {
		return find.query()
				.where()
					.eq("muscle_group_id", muscGroupId)
				.setMaxRows(MAX_ROWS)
				.findPagedList();
	}
	
	public static Exercise retrieveExerciseById(Long exerciseId) {
		return find.byId(exerciseId);
	}
	
	public static List<Exercise> retrieveRandomExercises() {
		String sql = "select id, name, image_url from exercise order by rand() limit " + MAX_ROWS;
		RawSql rawSql = 
				RawSqlBuilder
				.parse(sql)
				.create();
		Query<Exercise> query = Ebean.find(Exercise.class);
		query.setRawSql(rawSql);
		List<Exercise> exercises = query.findList();
		return exercises;
	} 
	
}
