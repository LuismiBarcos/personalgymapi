package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints.Required;

@Entity
public class ApiKey extends Model{
	
	private static final Finder<Long, ApiKey> find = new Finder<>(ApiKey.class); 
	
	@Id
	private Long id;
	@Required
	private String apiKey;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getApiKey() {
		return apiKey;
	}
	
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public static ApiKey findByHash(String hash) {
		if(hash == null) {
			return null;
		}
		return find.query()
				.where()
					.eq("api_key", hash)
				.findOne();
	}
}
