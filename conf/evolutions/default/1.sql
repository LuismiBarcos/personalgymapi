# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table api_key (
  id                            bigint auto_increment not null,
  api_key                       varchar(255),
  constraint pk_api_key primary key (id)
);

create table exercise (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  image_url                     varchar(255),
  description                   varchar(15000),
  muscle_group_id               bigint,
  constraint pk_exercise primary key (id)
);

create table muscle_group (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  image_url                     varchar(255),
  constraint pk_muscle_group primary key (id)
);

alter table exercise add constraint fk_exercise_muscle_group_id foreign key (muscle_group_id) references muscle_group (id) on delete restrict on update restrict;
create index ix_exercise_muscle_group_id on exercise (muscle_group_id);


# --- !Downs

alter table exercise drop foreign key fk_exercise_muscle_group_id;
drop index ix_exercise_muscle_group_id on exercise;

drop table if exists api_key;

drop table if exists exercise;

drop table if exists muscle_group;

